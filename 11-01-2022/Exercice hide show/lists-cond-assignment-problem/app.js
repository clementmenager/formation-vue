const app = Vue.createApp({
    data() {
        return { 
            nameTask: "",
            tasks: [],
            taskListVisible: true,
        };
    },
    computed:{
        buttonCaption(){
            return this.taskListVisible ? 'Hide List' : 'Show List'
        }
    },
    methods: {
        addTask(){
            this.tasks.push(this.nameTask)
        },
        toggleTaskList(){
            this.taskListVisible = !this.taskListVisible
        }
    }
});

app.mount('#assignment');
