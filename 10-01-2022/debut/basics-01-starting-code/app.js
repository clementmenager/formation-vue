const app = Vue.createApp({
    data() {
        return{
            messageA: 'Finish the course',
            // messageB: '<h2>Master Vue and build amazing app !</h2>',
            messageB: 'Master Vue and build amazing app !',
            vueLink: 'https://vuejs.org/'
        };
    },
    methods: {
        outputGoal(){
            const randomNumber = Math.random();
            if (randomNumber < 0.5){
                return this.messageA;
            }else{
                return this.messageB;
            }
        }
    }
});


app.mount('#user-goal');